package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestOscLoginAdd extends BaseTest{

	private AppiumDriver<MobileElement> driver;
//	private boolean isInstall = true;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="点击+登录成功")
	public void TestLoginAdd() throws InterruptedException {
		// 点击+
		MobileElement loginbtnadd = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet_pub"));
		loginbtnadd.click();
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		//进入登录页面
		MobileElement viewLogin = driver.findElement(By
				.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("========" + viewLogin.getText());
		//输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginUsername = driver.findElement(By.id("net.oschina.app:id/et_login_username"));
		loginUsername.sendKeys("xxxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		MobileElement loginBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();
		
		//刷新页面等待3秒
	    try{
	    	Thread.sleep(3000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    //进入登录成功后截图
	    this.screenshot("LoginAdd登录成功.jpg",driver);

		// 点击【我的】
		MobileElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMe.click();
		
		//进入登录成功的个人信息界面
		MobileElement loginName = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		Assert.assertEquals(loginName.getText(),"金龙鱼管家");
		if(loginName.getText().equals("金龙鱼管家") == true){
		System.out.println("====目前在个人信息界面登录人是" + loginName.getText());
		Reporter.log("点击+登录成功");
		}
		else{
			Reporter.log("点击+登录失败");
		}
		
		
		//注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);
	    //进入注销成功后截图
	    this.screenshot("LoginAdd注销成功.jpg",driver);		
		
	}

	@AfterClass
	public void tearDown() throws Exception {
		 driver.quit();
	}

}
