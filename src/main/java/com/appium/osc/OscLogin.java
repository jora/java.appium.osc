package com.appium.osc;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
public class OscLogin {
	public void login(AppiumDriver<MobileElement> driver){
		// 点击【我的】
		MobileElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMe.click();
		// 点击头像登录
		MobileElement loginbtn = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		loginbtn.click();
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		//进入登录页面
		MobileElement viewLogin = driver.findElement(By.id("net.oschina.app:id/iv_login_logo"));
//		MobileElement viewLogin = (new WebDriverWait(driver,40)).until(
//	    	new ExpectedCondition<MobileElement>(){  
//	    	@Override  
//	    		public MobileElement apply(WebDriver d) {
//		         MobileElement subViewLogin = driver.findElement(By.id("net.oschina.app:id/iv_login_logo"));
//		         return subViewLogin;
//	    	}
//	    	});
//		System.out.println("========" + viewLogin.getText());
		//输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginuUsername = driver.findElement(By.id("net.oschina.app:id/et_login_username"));
		loginuUsername.sendKeys("xxxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		MobileElement loginBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();
		
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		// 点击【我的】
		MobileElement itemMeTwo = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMeTwo.click();
		
		//进入登录成功的个人信息界面
		MobileElement loginName = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		if(loginName.getText().equals("金龙鱼管家") == true){
		System.out.println("====目前在个人信息界面登录人是" + loginName.getText());
		Reporter.log("点击+登录成功");
		}
	}
}
