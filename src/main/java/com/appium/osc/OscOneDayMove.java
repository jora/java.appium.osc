package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscOneDayMove extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-每日乱弹")
	public void OneDayMove() throws InterruptedException {
		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【每日乱弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(2);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();

		// 等待30秒内寻找这个元素
		 (new WebDriverWait(driver,30)).until(
		 new ExpectedCondition<MobileElement>(){
		 @Override
		 public MobileElement apply(WebDriver d) {
		     MobileElement recyclerView =
		 driver.findElement(By.id("net.oschina.app:id/recyclerView"));
		     List<MobileElement> tweet_Item =
		 recyclerView.findElements(By.id("net.oschina.app:id/tv_title"));
		 if(tweet_Item.size()>0){
		     MobileElement textView = tweet_Item.get(0);
		     return textView;
		 }
		     return null;
		 }
		 });

	   //进入每日乱弹列表后截图
		 this.screenshot("OneDayMove每日乱弹列表.jpg",driver);	
		// 等待20秒刷新页面
		// try{
		// Thread.sleep(20000);
		// }
		// catch(Exception e){
		// e.printStackTrace();
		// }

		// 点击第一条动弹
		MobileElement recyclerView =
		 driver.findElement(By.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> tweet_Item =
		 recyclerView.findElements(By.id("net.oschina.app:id/tv_title"));
		Assert.assertNotEquals(tweet_Item.size(), 0);		
		MobileElement textView = tweet_Item.get(0);
		String titleText = textView.getText().substring(14);
		System.out.println("=====每日乱弹：" + titleText);
	    textView.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		
		//刷新页面等待3秒
	    try{
	    	Thread.sleep(3000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }

		// 打印详情页面的标题
		MobileElement Container = driver.findElement(By
				.id("net.oschina.app:id/lay_nsv"));
		MobileElement inforTitle = Container.findElement(By
				.id("net.oschina.app:id/tv_title"));
		Assert.assertEquals(inforTitle.getText(),titleText);
		if(inforTitle.getText().equals(titleText) == true){
			System.out.println("====每日乱弹标题：" + inforTitle.getText());
			Reporter.log("动弹-每日乱弹-查看列表和详情信息成功");
		}else{
			Reporter.log("动弹-每日乱弹-查看列表和详情信息失败");
		}
		
		//进入每日乱弹详情界面后截图
	    this.screenshot("OneDayMove每日乱弹详情界面.jpg",driver);

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
