package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

public class OscFindOpenOtherSoftwave extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "发现-开源软件-分类-其他开源-区块链-ZCash")
	public void FindOpenOtherSoftwave() throws InterruptedException {
		// 点击【发现】
		MobileElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 点击【开源软件】
		MobileElement findSoft = driver.findElement(By
				.id("net.oschina.app:id/rl_soft"));
		findSoft.click();

		// 点击【分类】
		MobileElement tabstrip = driver.findElement(By
				.id("net.oschina.app:id/pager_tabstrip"));
		List<MobileElement> tab_title = tabstrip.findElements(By
				.id("net.oschina.app:id/tab_title"));
		if (tab_title.size() > 0) {
			MobileElement getTitle = tab_title.get(0);
			getTitle.click();
		}

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_catalog = driver.findElement(By
								.id("net.oschina.app:id/lv_catalog"));
						List<MobileElement> software_catalog_name = lv_catalog.findElements(By
								.id("net.oschina.app:id/tv_software_catalog_name"));
						if (software_catalog_name.size() > 0) {
							MobileElement getSoftware1 = software_catalog_name
									.get(software_catalog_name.size() - 1);
							return getSoftware1;
						}
						return null;
					}
				});
		// 进入开源软件-分类列表截图
		this.screenshot("FindOpenOtherSoftwave开源软件分类列表.jpg", driver);
		// 移动元素，显示最后一个分类
		MobileElement lv_catalog = (MobileElement) driver.findElement(By
				.id("net.oschina.app:id/lv_catalog"));
		List<MobileElement> software_catalog_name = lv_catalog.findElements(By
				.id("net.oschina.app:id/tv_software_catalog_name"));
		MobileElement getSoftware1 = software_catalog_name.get(0);
		MobileElement getSoftware9 = software_catalog_name.get(9);
		MobileElement getSoftwareLast = software_catalog_name
				.get(software_catalog_name.size() - 1);
		TouchAction touchAction = new TouchAction(driver);
		touchAction.press(getSoftware9).perform();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		touchAction.moveTo(getSoftware1).release().perform();
		Thread.sleep(5000);
		// 进入开源软件-分类列表移动到最后一个分类截图
		this.screenshot("FindOpenOtherSoftwave分类列表移动到最后一个分类.jpg", driver);
		// 分类-点击最后一个分类【其他开源】
		String getSoftwareText = getSoftwareLast.getText();
		System.out.println("=====最后一个分类：" + getSoftwareText);
		getSoftwareLast.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_tag = driver.findElement(By
								.id("net.oschina.app:id/lv_tag"));
						List<MobileElement> software_catalog_name2 = lv_tag.findElements(By
								.id("net.oschina.app:id/tv_software_catalog_name"));
						if (software_catalog_name2.size() > 0) {
							MobileElement getSoftware2 = software_catalog_name2
									.get(software_catalog_name2.size() - 1);
							return getSoftware2;
						}
						return null;
					}
				});
		// 进入开源软件-分类-其他开源列表截图
		this.screenshot("FindOpenOtherSoftwave开源软件分类其他开源列表.jpg", driver);
		// 移动元素，显示最后一个分类
		MobileElement lv_tag = driver.findElement(By
				.id("net.oschina.app:id/lv_tag"));
		List<MobileElement> software_catalog_name2 = lv_tag.findElements(By
				.id("net.oschina.app:id/tv_software_catalog_name"));
		MobileElement getOtherSource1 = software_catalog_name2.get(0);
		MobileElement getOtherSource9 = software_catalog_name2.get(9);
		MobileElement getOtherSourceLast = software_catalog_name2
				.get(software_catalog_name.size() - 1);
		TouchAction touchAction2 = new TouchAction(driver);
		touchAction2.press(getOtherSource9).perform();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		touchAction2.moveTo(getOtherSource1).release().perform();
		Thread.sleep(5000);
		// 进入开源软件-分类-其他开源列表移动到最后一个截图
		this.screenshot("FindOpenOtherSoftwave开源软件分类其他开源列表到最后一个.jpg", driver);
		// 分类-最后一个分类【其他开源】-点击【区块链】
		String blockChainText = getOtherSourceLast.getText();
		System.out.println("=====最后一种其他开源类：" + blockChainText);
		getOtherSourceLast.click();

		// 等待30秒内寻找这个元素
		MobileElement getZCash = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_software = driver.findElement(By
								.id("net.oschina.app:id/lv_software"));
						List<MobileElement> tv_title = lv_software
								.findElements(By
										.id("net.oschina.app:id/tv_title"));
						if (tv_title.size() > 0) {
							MobileElement getSoftware3 = tv_title.get(0);
							return getSoftware3;
						}
						return null;
					}
				});
		// 进入开源软件-分类-其他开源-区块链列表截图
		this.screenshot("FindOpenOtherSoftwave开源软件分类其他开源区块链列表.jpg", driver);
		// 分类-最后一个分类【其他开源】-点击【区块链】-点击ZCash
		String ZCashText = getZCash.getText();
		System.out.println("=====第一种其他开源：" + ZCashText);
		getZCash.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入开源软件-分类-其他开源-区块链列表-详情截图
		this.screenshot("FindOpenOtherSoftwave开源软件分类其他开源区块链详情.jpg", driver);
		// 打印详情页面的标题
		MobileElement tv_software_name = driver.findElement(By
				.id("net.oschina.app:id/tv_software_name"));
		System.out.println("=====详情标题：" + tv_software_name.getText());

		// 返回软件详情返回语言分类
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		Reporter.log("发现-开源软件-分类-其他开源-区块链-ZCash-查看软件详情信息成功");

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
