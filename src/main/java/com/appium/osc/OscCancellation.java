package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OscCancellation {
	public void Cancellation(AppiumDriver<MobileElement> driver) {
		// 点击【我的】
		MobileElement itemMeTwo = driver.findElement(By
				.id("net.oschina.app:id/nav_item_me"));
		itemMeTwo.click();

		// 点击设置，进入设置页面，注销
		MobileElement setting = driver.findElement(By
				.id("net.oschina.app:id/iv_logo_setting"));
		setting.click();
		MobileElement settingclose = driver.findElement(By.name("注销"));
		settingclose.click();

		// 点击<，返回个人信息界面，显示“点击头像登录”字样，即注销成功
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		MobileElement loginNameAssert = driver.findElement(By.name("点击头像登录"));
		if (loginNameAssert.getText().equals("点击头像登录") == true) {
			System.out.println("====注销成功====");
		} else {
			System.out.println("====注销失败====");
		}
	}
}
