package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscMyPrivateLetter extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	// private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "我的消息-私信")
	public void MyPrivateLetter() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【我的消息】
		MobileElement myNews = driver.findElement(By.name("我的消息"));
		myNews.click();
		// 进入消息中心页面
		MobileElement messageCenter = driver.findElement(By.name("消息中心"));
		System.out.println("=====进入" + messageCenter.getText());
		// 进入消息中心截图
		this.screenshot("MyPrivateLetter消息中心.jpg", driver);
		// 点击私信
		MobileElement myPrivateLetter = driver.findElement(By.name("私信"));
		myPrivateLetter.click();
		System.out.println("==进入我的私信列表==");
		// 进入私信列表截图
		this.screenshot("MyPrivateLetter私信列表.jpg", driver);
		// 遍历列表，点击第一条消息，进入私信内容
		List<MobileElement> linearLayouts = driver.findElements(By
				.className("android.widget.LinearLayout"));
		if (linearLayouts.size() > 0) {
			MobileElement textView = linearLayouts.get(0);
			MobileElement subTextView = textView.findElement(By
					.id("net.oschina.app:id/tv_user_name"));
			String userNameText = subTextView.getText();
			subTextView.click();
			
			// 进入私信内容页面
			MobileElement inforDetails = driver.findElement(By
					.name(userNameText));
			System.out.println("=====进入" + inforDetails.getText());
			// 进入私信内容后截图
			this.screenshot("MyPrivateLetter私信内容.jpg", driver);
			// 点击<，从私信内容返回消息中心界面
			MobileElement returnMyPrivateLetter = driver.findElement(By
					.id("net.oschina.app:id/action_bar"));
			MobileElement clickReturnMyPrivateLetter = returnMyPrivateLetter
					.findElement(By.className("android.widget.ImageButton"));
			clickReturnMyPrivateLetter.click();
		} else {
			System.out.println("====暂无内容====");
		}

		Reporter.log("我的消息-私信");

		// 点击<，从消息中心返回个人信息界面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
