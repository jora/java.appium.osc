package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscLoginNewsCollection extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "综合-开源资讯-收藏")
	public void LoginNewsCollection() throws InterruptedException {
		this.screenshot("LoginNewsCollection1.jpg", driver);
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);
		this.screenshot("LoginNewsCollection2.jpg", driver);
		// 点击【综合】
		WebElement itemMe = driver.findElement(By
				.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();
		
		// 点击【开源资讯】
		WebElement layoutView = driver.findElement(By
				.id("net.oschina.app:id/layout_tab"));
		WebElement linearLayout = layoutView.findElement(By
				.className("android.widget.LinearLayout"));
		List<WebElement> support = linearLayout.findElements(By
				.className("android.support.v7.a.a$f"));
		WebElement supportText = support.get(0);
		supportText.click();
		
		// 等待30秒内寻找这个元素
		   (new WebDriverWait(driver,20)).until(
			new ExpectedCondition<MobileElement>(){
				@Override
				public MobileElement apply(WebDriver d) {
					MobileElement recyclerView = driver.findElement(By
							.id("net.oschina.app:id/recyclerView"));
					List<MobileElement> titleView = recyclerView.findElements(By
							.id("net.oschina.app:id/ll_title"));
					   if(titleView.size()>0){
					   MobileElement textView=titleView.get(0);
					   return textView;
					   }
					   return null;
				 }
			});		
		 //进入开源资讯列表界面后截图
		   this.screenshot("LoginNewsCollection3.jpg", driver);		   
		// 点击资讯标题
		   MobileElement recyclerView = driver.findElement(By
					.id("net.oschina.app:id/recyclerView"));
			List<MobileElement> titleView = recyclerView.findElements(By
					.id("net.oschina.app:id/ll_title"));
//			String titleText = "";
			for(int i=0;i<titleView.size();i++){
				MobileElement textView=titleView.get(i);				
				MobileElement lay_info = textView.findElement(By.id("net.oschina.app:id/lay_info"));
				if(lay_info != null){
				MobileElement subTextView = textView.findElement(By
						   .id("net.oschina.app:id/tv_title"));
				   String titleText = subTextView.getText().substring(7);
				   System.out.println("=====列表标题：" + titleText );
				   subTextView.click();
				   break;
				}
			}
		      
		   
		 //进入详情页面
		   MobileElement actionBar = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		   MobileElement actionBarTexts = actionBar.findElement(By.className("android.widget.TextView"));
		   String actionBarText = actionBarTexts.getText();
		   System.out.println("=====进入" + actionBarText);
		  
			// 等待30秒内寻找这个元素
		   (new WebDriverWait(driver,30)).until(
			new ExpectedCondition<MobileElement>(){
				@Override
				public MobileElement apply(WebDriver d) {
					MobileElement lay_container = driver.findElement(By
							.id("net.oschina.app:id/lay_container"));				
				      return lay_container;
				 }
			});	  
		   
		   //打印详情页面的标题
		   String inforDetailsTitle = "";
		   if(actionBarText.equals("软件详情") == true){
			   MobileElement tv_software_name = driver.findElement(By
					   .id("net.oschina.app:id/tv_software_name"));
			   inforDetailsTitle = tv_software_name.getText().substring(13);
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else if(actionBarText.equals("问答详情") == true){
			   MobileElement tv_ques_detail_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_ques_detail_title"));
			   inforDetailsTitle = tv_ques_detail_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else{
			   MobileElement tv_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_title"));
			   inforDetailsTitle = tv_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }
		// 进入开源资讯详情界面后截图
		   this.screenshot("LoginNewsCollection4.jpg", driver);

		// 收藏成功
		WebElement collectionLinear = driver.findElement(By
				.className("android.widget.LinearLayout"));
		WebElement clickCollection = collectionLinear.findElement(By
				.id("net.oschina.app:id/ib_fav"));
		clickCollection.click();

		//刷新页面等待3秒
	    try{
	    	Thread.sleep(3000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		// 收藏成功截图
	    this.screenshot("LoginNewsCollection5.jpg", driver);

		// 从博客详情返回综合列表
		WebElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		WebElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		this.screenshot("LoginNewsCollection6.jpg", driver);
		// 检测收藏标题是否存在于我的收藏列表中
		OscCollectionResultAssert ResultAssert = new OscCollectionResultAssert();
		ResultAssert.CollectionResultAssert(driver, inforDetailsTitle);
		// 进入我的收藏列表后截图
		this.screenshot("LoginNewsCollection7.jpg", driver);
		Reporter.log("综合-开源资讯-登录-收藏");

		// 从我的收藏列表返回到个人信息页面
		MobileElement returnButton2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton2 = returnButton2.findElement(By
				.className("android.widget.ImageButton"));
		clickButton2.click();
		this.screenshot("LoginNewsCollection8.jpg", driver);
		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);
		this.screenshot("LoginNewsCollection9.jpg", driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
