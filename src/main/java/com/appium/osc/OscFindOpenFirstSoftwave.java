package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OscFindOpenFirstSoftwave extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "发现-开源软件-分类-编程语言-java-Spring")
	public void FindOpenFirstSoftwave() throws InterruptedException {
		// 点击【发现】
		MobileElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 点击【开源软件】
		MobileElement findSoft = driver.findElement(By
				.id("net.oschina.app:id/rl_soft"));
		findSoft.click();

		// 点击【分类】
		MobileElement tabstrip = driver.findElement(By
				.id("net.oschina.app:id/pager_tabstrip"));
		List<MobileElement> tab_title = tabstrip.findElements(By
				.id("net.oschina.app:id/tab_title"));
		if (tab_title.size() > 0) {
			MobileElement getTitle = tab_title.get(0);
			getTitle.click();
		}

		// 等待30秒内寻找这个元素
		MobileElement getSoftware = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_catalog = driver.findElement(By
								.id("net.oschina.app:id/lv_catalog"));
						List<MobileElement> software_catalog_name = lv_catalog.findElements(By
								.id("net.oschina.app:id/tv_software_catalog_name"));
						if (software_catalog_name.size() > 0) {
							MobileElement getSoftware1 = software_catalog_name
									.get(0);
							return getSoftware1;
						}
						return null;
					}
				});
		// 进入开源软件-分类列表截图
		this.screenshot("FindOpenFirstSoftwave开源软件分类列表.jpg", driver);
		// 分类-第一分类【编程语言】
		String banTitleText = getSoftware.getText();
		System.out.println("=====第一分类：" + banTitleText);
		getSoftware.click();

		// 等待30秒内寻找这个元素
		MobileElement getSoftJava = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_tag = driver.findElement(By
								.id("net.oschina.app:id/lv_tag"));
						List<MobileElement> software_catalog_name2 = lv_tag.findElements(By
								.id("net.oschina.app:id/tv_software_catalog_name"));
						if (software_catalog_name2.size() > 0) {
							MobileElement getSoftware2 = software_catalog_name2
									.get(0);
							return getSoftware2;
						}
						return null;
					}
				});
		// 进入开源软件-分类-编程语言列表截图
		this.screenshot("FindOpenFirstSoftwave开源软件分类编程语言列表.jpg", driver);
		// 分类-第一分类【编程语言】-点击【java】
		String softJavaText = getSoftJava.getText();
		System.out.println("=====第一种语言类：" + softJavaText);
		getSoftJava.click();

		// 等待30秒内寻找这个元素
		MobileElement getSpring = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lv_software = driver.findElement(By
								.id("net.oschina.app:id/lv_software"));
						List<MobileElement> tv_title = lv_software
								.findElements(By
										.id("net.oschina.app:id/tv_title"));
						if (tv_title.size() > 0) {
							MobileElement getSoftware3 = tv_title.get(0);
							return getSoftware3;
						}
						return null;
					}
				});
		// 进入开源软件-分类-编程语言列表截图
		this.screenshot("FindOpenFirstSoftwave开源软件分类编程语言java列表.jpg", driver);
		// 分类-第一分类【编程语言】-点击【java】-点击Spring
		String softSpringText = getSpring.getText();
		System.out.println("=====第一种语言：" + softSpringText);
		getSpring.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入开源软件-分类-编程语言-java列表截图
		this.screenshot("FindOpenFirstSoftwave开源软件分类编程语言java列表Spring详情.jpg",
				driver);
		// 打印详情页面的标题
		MobileElement tv_software_name = driver.findElement(By
				.id("net.oschina.app:id/tv_software_name"));
		System.out.println("=====详情标题：" + tv_software_name.getText());

		// 返回软件详情返回语言分类
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		Reporter.log("发现-开源软件-分类-编程语言-java-Spring-查看软件详情信息成功");

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
