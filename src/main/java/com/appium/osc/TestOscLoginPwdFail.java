package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestOscLoginPwdFail extends BaseTest{

	private AppiumDriver<MobileElement> driver;
//	private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="登录失败-密码错误")
	public void TestLoginPwdFail() throws InterruptedException {
		// 点击【我的】
		MobileElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMe.click();
		// 点击头像登录
		MobileElement loginbtn = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		loginbtn.click();
		//进入登录页面
		MobileElement viewLogin = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("========" + viewLogin.getText());
		//输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginUsername = driver.findElement(By.id("net.oschina.app:id/et_login_username"));
		loginUsername.sendKeys("xxxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("654321");
		MobileElement loginBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();

		//刷新页面等待2秒
	    try{
	    	Thread.sleep(2000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    //进入登录失败报错后截图
	    this.screenshot("LoginPwdFail登录失败报错.jpg",driver);
	    
		//判断是否进入登录成功的个人信息界面，在界面还是找到登录按钮，则登录失败
	    MobileElement assertLogin = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
	    Assert.assertEquals(assertLogin.getText(),"登录");
	    if(assertLogin.getText().equals("登录") == true){
		   System.out.println("==密码错误==");
		   Reporter.log("登录失败-密码错误-此用例通过");
		}else{
			Reporter.log("此用例执行失败");
		}
		
		
		
	}


	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
