package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class OscAddColumn extends BaseTest{

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="综合-增加栏目")
	public void AddColumn() throws InterruptedException {
		// 点击【综合】
		WebElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();  
		   
		// 点击+
		WebElement arrowDown = driver.findElement(By.id("net.oschina.app:id/iv_arrow_down"));
        arrowDown.click();
        
        //进入切换栏目页面
		WebElement operatorColumn = driver.findElement(By.id("net.oschina.app:id/tv_operator"));
		System.out.println("========" + operatorColumn.getText());
		
		//增加栏目
		WebElement recyclerInactive = driver.findElement(By.id("net.oschina.app:id/view_recycler_inactive"));
		List<WebElement> textViews = recyclerInactive.findElements(By.className("android.widget.TextView"));
		   WebElement clickAddColumn = textViews.get(0);
		   String addColumnText = clickAddColumn.getText();
		   System.out.println("====" + addColumnText + "====");
		   clickAddColumn.click();
		   
			// 等待30秒内寻找这个元素
		   WebElement viewAddSuccess =(new WebDriverWait(driver,20)).until(
			 new ExpectedCondition<WebElement>(){
			 @Override
			 public WebElement apply(WebDriver d) {
				 WebElement recyclerActive = driver.findElement(By.id("net.oschina.app:id/view_recycler_active"));
					List<WebElement> frameLayoutResult = recyclerActive.findElements(By.className("android.widget.TextView"));
				    WebElement addSuccess = frameLayoutResult.get(frameLayoutResult.size()-1);
					return addSuccess;
			     }
		     });	   
		
		//判断是否增加栏目成功
		if(viewAddSuccess.getText().equals(addColumnText) == true){
		   System.out.println("====增加栏目:" + viewAddSuccess.getText() + "成功");
		}
		
		// 点击关闭，返回首页
		WebElement arrowDownColse = driver.findElement(By.id("net.oschina.app:id/iv_arrow_down"));
		arrowDownColse.click();
		
		// 等待30秒内寻找这个元素
		WebElement assertSupportText = (new WebDriverWait(driver,20)).until(
			 new ExpectedCondition<WebElement>(){
			 @Override
			 public WebElement apply(WebDriver d) {
				 WebElement layoutView = driver.findElement(By.id("net.oschina.app:id/layout_tab"));
					List<WebElement> support = layoutView.findElements(By.className("android.widget.TextView"));
					   WebElement supportText=support.get(support.size()-1);
                       return supportText;
			 }
		});	  
		 
		//移动元素，再比较
		MobileElement layoutView = (MobileElement)driver.findElement(By.id("net.oschina.app:id/layout_tab"));
		List<MobileElement> support = layoutView.findElements(By.className("android.widget.TextView"));
		     MobileElement supportText1=support.get(0);
		     MobileElement supportText4=support.get(3);
		     MobileElement supportTextLast=support.get(support.size()-1);
	        TouchAction touchAction = new TouchAction(driver);
	        touchAction.press(supportText4).perform();
	        try {
	            Thread.sleep(5000);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	        touchAction.moveTo(supportText1).release().perform();
	        Thread.sleep(5000);
	        Assert.assertEquals(supportTextLast.getText(),addColumnText);
		
		// 检测首页是否显示增加的栏目
		if(assertSupportText.getText().equals(addColumnText) == true){
		   System.out.println("====显示栏目:" + assertSupportText.getText() + "成功");
		   Reporter.log("增加栏目成功");
		}else{
			Reporter.log("增加栏目失败");
		}
		
	}

	@AfterClass
	public void tearDown() throws Exception {
		 driver.quit();
	}


	
}
