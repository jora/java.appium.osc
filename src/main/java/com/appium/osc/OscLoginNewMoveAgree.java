package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscLoginNewMoveAgree extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-最新动弹-登录-点赞成功")
	public void LoginNewMoveAgree() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【最新动弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(0);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();

		// 等待30秒内寻找这个元素
		MobileElement getTweetNames = (new WebDriverWait(driver, 30))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement recyclerView = driver.findElement(By
								.id("net.oschina.app:id/recyclerView"));
						List<MobileElement> tweet_items = recyclerView.findElements(By
								.id("net.oschina.app:id/tv_tweet_name"));
						if (tweet_items.size() > 0) {
							MobileElement textView = tweet_items.get(0);
							return textView;
						}
						return null;
					}
				});
		// 进入最新动弹列表界面后截图
		this.screenshot("NoLoginNewMoveAgree最新动弹列表.jpg", driver);
		// 点击第一条动弹
		String titleText = getTweetNames.getText();
		System.out.println("=====动弹标题：" + titleText);
		getTweetNames.click();

		// 进入动弹详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement Container = driver.findElement(By
				.id("net.oschina.app:id/layout_container"));
		MobileElement inforDetailsContent = Container.findElement(By
				.id("net.oschina.app:id/tv_content"));
		System.out.println("=====详情标题：" + inforDetailsContent.getText());
		// 进入详情界面后截图
		this.screenshot("NoLoginNewMoveAgree详情界面.jpg", driver);
		// 记录下点赞前的数量
		MobileElement getLikeCountss = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> getLikeCounts = getLikeCountss.findElements(By
				.className("android.widget.TextView"));
		MobileElement getLikeCount = getLikeCounts.get(0);
		String likeCount1 = getLikeCount.getText().substring(2,
				getLikeCount.getText().length() - 1);
		System.out.println("==点赞前数量：" + likeCount1);
		// 进入点赞前数量截图
		this.screenshot("NoLoginNewMoveAgree点赞前数量.jpg", driver);
		// 点击【点赞】
		MobileElement clickThumbup = driver.findElement(By
				.id("net.oschina.app:id/iv_thumbup"));
		clickThumbup.click();

		// 比较下点赞数是否增加1，是，则点赞成功
		String likeCount2 = getLikeCount.getText().substring(2,
				getLikeCount.getText().length() - 1); // 记录下点赞后的数量
		System.out.println("==点赞后数量：" + likeCount2);
		if (Integer.valueOf(likeCount2) > Integer.valueOf(likeCount1)) {
			System.out.println("==点赞成功==");
			Reporter.log("动弹-最新动弹-登录-点赞成功");
		}
		// 进入点赞后数量截图
		this.screenshot("NoLoginNewMoveAgree点赞后数量.jpg", driver);
		// 返回最新动弹页面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
