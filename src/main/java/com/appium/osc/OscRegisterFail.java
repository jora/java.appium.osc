package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscRegisterFail extends BaseTest{

	private AppiumDriver<MobileElement> driver;
//	private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="注册失败-验证码错误")
	public void RegisterFail() throws InterruptedException {
		// 点击【我的】
		MobileElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMe.click();
		// 点击头像登录
		MobileElement loginbtn = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		loginbtn.click();
		//进入登录页面
		MobileElement viewLogin = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("========" + viewLogin.getText());
		
		// 点击注册
		MobileElement registerBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_register"));
		registerBtn.click();
		//输入手机号和密码，登录
		MobileElement registerUsername = driver.findElement(By.id("net.oschina.app:id/et_register_username"));
		registerUsername.sendKeys("13266170816");
		MobileElement authCode = driver.findElement(By.id("net.oschina.app:id/et_register_auth_code"));
		authCode.sendKeys("654321");
		MobileElement registerSubmit = driver.findElement(By.id("net.oschina.app:id/bt_register_submit"));
		registerSubmit.click();

		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    //进入注册失败报错后截图
	    this.screenshot("RegisterFail注册失败报错.jpg",driver);
	    
		//判断是否进入注册成功的个人信息界面，在界面还是找到注册按钮，则注册失败
	    MobileElement registerName = driver.findElement(By.id("net.oschina.app:id/bt_register_submit"));
	    Assert.assertEquals(registerName.getText(),"注册");
		if(registerName.getText().equals("注册") == true){
		   System.out.println("==注册失败-验证码错误==");
		   Reporter.log("注册失败-验证码错误-此用例通过");
		}else{
		   Reporter.log("此用例执行失败");
		}
		
		
		
	}


	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
