package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestOscCancellation extends BaseTest{

	private AppiumDriver<MobileElement> driver;
//	private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="注销成功")
	public void TestCancellation() throws InterruptedException {
		// 点击【我的】
		MobileElement itemMe = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMe.click();
		// 点击头像登录
		MobileElement loginbtn = driver.findElement(By.id("net.oschina.app:id/tv_nick"));
		loginbtn.click();
		//进入登录页面
		MobileElement viewLogin = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("========" + viewLogin.getText());

		//输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginuUsername = driver.findElement(By.id("net.oschina.app:id/et_login_username"));
		loginuUsername.sendKeys("xxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		MobileElement loginBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();

		// 点击【我的】
		MobileElement itemMeTwo = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMeTwo.click();
		//进入登录成功的个人信息界面
		MobileElement loginName = driver.findElement(By.name("金龙鱼管家"));
		System.out.println("=====" + loginName.getText());
	    //进入登录成功后截图
	    this.screenshot("Cancellation登录成功.jpg",driver);
		
		//点击设置，进入设置页面，注销
		MobileElement setting = driver.findElement(By.id("net.oschina.app:id/iv_logo_setting"));
		setting.click();
		MobileElement settingclose = driver.findElement(By.name("注销"));
		settingclose.click();
	    //进入设置界面点击注销后截图
	    this.screenshot("Cancellation注销成功.jpg",driver);
		
		//点击<，返回个人信息界面，显示“点击头像登录”字样，即注销成功
		MobileElement returnButton = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By.className("android.widget.ImageButton"));
		clickButton.click();
		MobileElement loginNameAssert = driver.findElement(By.name("点击头像登录"));
		Assert.assertEquals(loginNameAssert.getText(), "点击头像登录");
		if(loginNameAssert.getText().equals("点击头像登录") == true){
		   System.out.println("==注销成功==" );
		   Reporter.log("注销成功");
		}else{
		   Reporter.log("注销失败");
		}
		
	}


	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
