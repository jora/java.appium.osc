package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class OscCollectionResultAssert {
	public void CollectionResultAssert(AppiumDriver<MobileElement> driver,String Title){
		// 点击【我的】
		WebElement itemMeTwo = driver.findElement(By.id("net.oschina.app:id/nav_item_me"));
		itemMeTwo.click();
		
		//检测我的收藏里是否增加以上博文标题，存在则收藏成功
		WebElement lay_about_info = driver.findElement(By.id("net.oschina.app:id/lay_about_info"));
		WebElement clickfavorite = lay_about_info.findElement(By.id("net.oschina.app:id/ly_favorite"));
		clickfavorite.click();
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		MobileElement recyclerViews = driver.findElement(By.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> tv_title2 = recyclerViews.findElements(By.id("net.oschina.app:id/tv_title"));
		Assert.assertNotEquals(tv_title2.size(), 0);
		MobileElement textView2=tv_title2.get(0);
		String userNameText2 = textView2.getText();
		if (userNameText2.indexOf(Title)!= -1) {
		     System.out.println("==收藏成功==");
		     Reporter.log("收藏成功");
		}else{
		    System.out.println("==收藏列表找不到收藏的记录==");
		    Reporter.log("收藏失败");
		}
		
	}
}
