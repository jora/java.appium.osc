package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscLoginNewMoveComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-最新动弹-登录-评论成功")
	public void LoginNewMoveComment() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【最新动弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(0);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();

		// 等待30秒内寻找这个元素
		 MobileElement getTweetName = (new WebDriverWait(driver,30)).until(
				 new ExpectedCondition<MobileElement>(){
				 @Override
				 public MobileElement apply(WebDriver d) {
				     MobileElement recyclerView =
				 driver.findElement(By.id("net.oschina.app:id/recyclerView"));
				     List<MobileElement> LinearLayouts =
				 recyclerView.findElements(By.className("android.widget.LinearLayout"));
				 if(LinearLayouts.size()>0){
				     MobileElement textView = LinearLayouts.get(0);
				     MobileElement tv_tweet_name =textView.findElement(By.id("net.oschina.app:id/tv_tweet_name"));
				     return tv_tweet_name;
				 }
				    return null;
				 }
				 });
		// 进入最新动弹列表界面后截图
		this.screenshot("LoginNewMoveComment最新动弹列表.jpg", driver);
		// 点击第一条动弹
		String titleText = getTweetName.getText();
		System.out.println("=====动弹标题：" + titleText);
		getTweetName.click();

		// 进入动弹详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement Container = driver.findElement(By
				.id("net.oschina.app:id/layout_container"));
		MobileElement inforDetailsContent = Container.findElement(By
				.id("net.oschina.app:id/tv_content"));
		System.out.println("=====详情标题：" + inforDetailsContent.getText());
		// 进入详情界面后截图
		this.screenshot("LoginNewMoveComment详情界面.jpg", driver);

		// 点击添加评论
		MobileElement commentContainer = driver.findElement(By
				.id("net.oschina.app:id/layout_container"));
		MobileElement clickComment = commentContainer.findElement(By
				.id("net.oschina.app:id/iv_comment"));
		clickComment.click();

		// 输入评论内容，提交评论成功
		MobileElement commentLinear = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement sendComment = commentLinear.findElement(By
				.id("net.oschina.app:id/et_comment"));
		sendComment.sendKeys("thumb up");
		// 输入内容后，按回车键，确定输入内容
		String cmdstr = "adb shell input keyevent 66";
		try {
			Runtime.getRuntime().exec(cmdstr).waitFor();
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MobileElement subComment = driver.findElement(By
				.id("net.oschina.app:id/btn_comment"));
		subComment.click();
		// 评论成功后截图
		this.screenshot("LoginNewMoveComment评论成功.jpg", driver);
		Reporter.log("动弹-最新动弹-登录-评论");

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
