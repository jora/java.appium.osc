package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {

	public AppiumDriver<MobileElement> getAppiumParameter() throws Exception{
		     AppiumDriver<MobileElement> driver;
		      // 配置appium相关参数
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", "f0bcade5");
				capabilities.setCapability("automationName", "Appium");
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("platformVersion", "4.4.4");

				// 配置测试apk
				capabilities.setCapability("appPackage", "net.oschina.app");
				capabilities.setCapability("appActivity","net.oschina.app.LaunchActivity");
				capabilities.setCapability("appWaitActivity","net.oschina.app.LaunchActivity"); //A new session could not be created的解决
				capabilities.setCapability("sessionOverride", true); // 每次启动时覆盖session，否则第二次后运行会报错不能新建session
//				capabilities.setCapability("unicodeKeyboard", true); // 设置键盘
//				capabilities.setCapability("resetKeyboard", false); // 设置默认键盘为appium的键盘

				// 如果真机设备已经安装，则不需要重新安装
//					File classpathRoot = new File(System.getProperty("user.dir"));
//					File appDir = new File(classpathRoot, "apps");
//					File app = new File(appDir,
//							"osc-android-v2.6.9-oschina-release.apk");
//					capabilities.setCapability("app", app.getAbsolutePath());

				// 启动appium
				driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				return driver;
	}
	
	//运行结果截图
	public void screenshot(String fileName, AppiumDriver<MobileElement> driver){
		try{
			File screenFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenFile, new File("screen/" + fileName));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
