package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscHotMove extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-热门动弹")
	public void HotMove() throws InterruptedException {
		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【热门动弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(1);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 30))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement recyclerView = driver.findElement(By
								.id("net.oschina.app:id/recyclerView"));
						List<MobileElement> tweet_Item = recyclerView
								.findElements(By
										.id("net.oschina.app:id/tweet_item"));
						if (tweet_Item.size() > 0) {
							MobileElement textView = tweet_Item.get(0);
							return textView;
						}
						return null;
					}
				});
		// 进入热门动弹列表截图
		this.screenshot("HotMove热门动弹列表.jpg", driver);

		// 等待20秒刷新页面
		// try{
		// Thread.sleep(20000);
		// }
		// catch(Exception e){
		// e.printStackTrace();
		// }

		// 点击第一条动弹
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> tweet_Item = recyclerView.findElements(By
				.id("net.oschina.app:id/tweet_item"));
		if (tweet_Item.size() > 0) {
			MobileElement textView = tweet_Item.get(0);
			String titleText = textView.getText();
			System.out.println("=====热门动弹：" + titleText);
			textView.click();
		}

		// 进入动弹详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入热门动弹详情截图
		this.screenshot("HotMove热门动弹详情.jpg", driver);
		// 打印详情页面的标题
		MobileElement Container = driver.findElement(By
				.id("net.oschina.app:id/layout_container"));
		MobileElement inforDetailsContent = Container.findElement(By
				.id("net.oschina.app:id/tv_content"));
		System.out.println("=====热门标题：" + inforDetailsContent.getText());
		Reporter.log("动弹-热门动弹");

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
