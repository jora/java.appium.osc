package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscViewNews extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "综合-开源资讯")
	public void ViewNews() throws InterruptedException {
		// 点击【综合】
		MobileElement itemMe = driver.findElement(By
				.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();
		// 点击【开源资讯】
		MobileElement layoutView = driver.findElement(By
				.id("net.oschina.app:id/layout_tab"));
		MobileElement linearLayout = layoutView.findElement(By
				.className("android.widget.LinearLayout"));
		List<MobileElement> support = linearLayout.findElements(By
				.className("android.support.v7.a.a$f"));
		MobileElement supportText = support.get(0);
		supportText.click();

		// 等待30秒内寻找这个元素
		   (new WebDriverWait(driver,20)).until(
			new ExpectedCondition<MobileElement>(){
				@Override
				public MobileElement apply(WebDriver d) {
					MobileElement recyclerView = driver.findElement(By
							.id("net.oschina.app:id/recyclerView"));
					List<MobileElement> titleView = recyclerView.findElements(By
							.id("net.oschina.app:id/ll_title"));
					   if(titleView.size()>0){
					   MobileElement textView=titleView.get(0);
					   return textView;
					   }
					   return null;
				 }
			});		
		 //进入开源资讯列表界面后截图
		   this.screenshot("NoLoginNewsComment开源资讯列表.jpg",driver);		   
		// 点击资讯标题
		   MobileElement recyclerView = driver.findElement(By
					.id("net.oschina.app:id/recyclerView"));
			List<MobileElement> titleView = recyclerView.findElements(By
					.id("net.oschina.app:id/ll_title"));
//			String titleText = "";
			for(int i=0;i<titleView.size();i++){
				MobileElement textView=titleView.get(i);				
				MobileElement lay_info = textView.findElement(By.id("net.oschina.app:id/lay_info"));
				if(lay_info != null){
				MobileElement subTextView = textView.findElement(By
						   .id("net.oschina.app:id/tv_title"));
				   String titleText = subTextView.getText().substring(7);
				   System.out.println("=====列表标题：" + titleText );
				   subTextView.click();
				   break;
				}
			}
		      
		   
		 //进入详情页面
		   MobileElement actionBar = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		   MobileElement actionBarTexts = actionBar.findElement(By.className("android.widget.TextView"));
		   String actionBarText = actionBarTexts.getText();
		   System.out.println("=====进入" + actionBarText);
		  
			// 等待30秒内寻找这个元素
//		   (new WebDriverWait(driver,30)).until(
//			new ExpectedCondition<MobileElement>(){
//				@Override
//				public MobileElement apply(WebDriver d) {
//					MobileElement lay_container = driver.findElement(By
//							.id("net.oschina.app:id/lay_container"));				
//				      return lay_container;
//				 }
//			});	  
		 //刷新页面等待5秒
		    try{
		    	Thread.sleep(5000);
		    }
		    catch(Exception e){
		    	e.printStackTrace();
		    }
		   //打印详情页面的标题
		   String inforDetailsTitle = "";
		   if(actionBarText.equals("软件详情") == true){
			   MobileElement tv_software_name = driver.findElement(By
					   .id("net.oschina.app:id/tv_software_name"));
			   inforDetailsTitle = tv_software_name.getText().substring(13);
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else if(actionBarText.equals("问答详情") == true){
			   MobileElement tv_ques_detail_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_ques_detail_title"));
			   inforDetailsTitle = tv_ques_detail_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else{
			   MobileElement tv_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_title"));
			   inforDetailsTitle = tv_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }
		
		//刷新页面等待3秒
	    try{
	    	Thread.sleep(3000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    //进入开源资讯详情界面后截图
	    this.screenshot("ViewNews开源资讯详情界面.jpg",driver);
	    
//	    HashMap<String, Double> swipeObj = new HashMap<String, Double> ();
//        swipeObj.put("startX", 0.0);
//        swipeObj.put("startY", 0.0);
//        swipeObj.put("endX", 0.0);
//        //startY = startY + 200;
//        swipeObj.put("endY", 200.0);
//        swipeObj.put("duration", 1.1);
	    //滑动屏幕，查看详情内容，直接找到【相关推荐】元素为止
		MobileElement commentEle = (new WebDriverWait(driver, 60))
		.until(new ExpectedCondition<MobileElement>() {
			@Override
			public MobileElement apply(WebDriver d) {
//				driver.swipe(100,800,200,400, 1000);
				int width = driver.manage().window().getSize().width;
				int height = driver.manage().window().getSize().height;
				driver.swipe(width / 2, height * 9/10, width /2 , height/10, 500);
				System.out.println("scroll=======" + height * 3/ 4 + " " + height /8);
//				int width = driver.manage().window().getSize().width;
//				int height = driver.manage().window().getSize().height;
//				driver.swipe(width * 3 / 4 , height / 2, width / 4, height / 2, 1000);
				
//				JavascriptExecutor js = (JavascriptExecutor) driver;
//				js.executeScript("mobile: swipe", swipeObj);
//		        swipeObj.put("startX", 0.0);
//		        swipeObj.put("startY", swipeObj.get("endY"));
//		        swipeObj.put("endX", 0.0);
//		        //startY = startY + 200;
//		        swipeObj.put("endY", swipeObj.get("endY") + 200);
//		        swipeObj.put("duration", 1.1);
		        
				MobileElement tv_blog_detail_about = driver.findElement(By
						.id("net.oschina.app:id/tv_blog_detail_about"));
				return tv_blog_detail_about;
			}
		});

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
