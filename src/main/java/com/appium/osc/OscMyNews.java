package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscMyNews extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	// private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "我的消息@我的信息")
	public void MyNews() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【我的消息】
		MobileElement myNews = driver.findElement(By.name("我的消息"));
		myNews.click();
		// 进入消息中心页面
		MobileElement messageCenter = driver.findElement(By.name("消息中心"));
		System.out.println("=====进入" + messageCenter.getText());
		// 进入消息中心截图
		this.screenshot("MyNews消息中心.jpg", driver);
		// 点击@我
		MobileElement myNews2 = driver.findElement(By.name("@我"));
		myNews2.click();
		System.out.println("==进入@我列表==" );
		// 进入@我列表截图
		this.screenshot("MyPrivateLetter@我列表.jpg", driver);
		// 遍历列表，点击第一条消息，进入详情
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> linearLayouts = recyclerView.findElements(By
				.className("android.widget.LinearLayout"));
		if (linearLayouts.size() > 0) {
			MobileElement textView = linearLayouts.get(0);
			MobileElement subTextView = textView.findElement(By
					.id("net.oschina.app:id/tv_origin"));
			subTextView.click();
			// 进入详情页面
			MobileElement inforDetails = driver.findElement(By.name("资讯详情"));
			System.out.println("=====进入" + inforDetails.getText());
			// 进入详情截图
			this.screenshot("MyPrivateLetter@我详情.jpg", driver);
			// 点击<，从资讯详情返回消息中心界面
			MobileElement returnMyNews = driver.findElement(By
					.id("net.oschina.app:id/action_bar"));
			MobileElement clickReturnMyNews = returnMyNews.findElement(By
					.className("android.widget.ImageButton"));
			clickReturnMyNews.click();
		} else {
			System.out.println("====暂无内容====");
		}

		Reporter.log("我的消息@我的信息");

		// 点击<，从消息中心返回个人信息界面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
