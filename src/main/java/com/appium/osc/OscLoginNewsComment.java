package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscLoginNewsComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "综合-开源资讯-登录-评论成功")
	public void LoginNewsComment() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【综合】
		MobileElement itemMe = driver.findElement(By
				.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();
		// 点击【开源资讯】
		MobileElement layoutView = driver.findElement(By
				.id("net.oschina.app:id/layout_tab"));
		MobileElement linearLayout = layoutView.findElement(By
				.className("android.widget.LinearLayout"));
		List<MobileElement> support = linearLayout.findElements(By
				.className("android.support.v7.a.a$f"));
		MobileElement supportText = support.get(0);
		supportText.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement recyclerView = driver.findElement(By
								.id("net.oschina.app:id/recyclerView"));
						List<MobileElement> titleView = recyclerView
								.findElements(By
										.id("net.oschina.app:id/ll_title"));
						if (titleView.size() > 0) {
							MobileElement textView = titleView.get(0);
							return textView;
						}
						return null;
					}
				});
		// 进入开源资讯列表界面后截图
		this.screenshot("LoginNewsComment开源资讯列表.jpg", driver);
		// 点击资讯标题
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> titleView = recyclerView.findElements(By
				.id("net.oschina.app:id/ll_title"));
		// String titleText = "";
		for (int i = 0; i < titleView.size(); i++) {
			MobileElement textView = titleView.get(i);
			MobileElement lay_info = textView.findElement(By
					.id("net.oschina.app:id/lay_info"));
			if (lay_info != null) {
				MobileElement subTextView = textView.findElement(By
						.id("net.oschina.app:id/tv_title"));
				String titleText = subTextView.getText().substring(7);
				System.out.println("=====列表标题：" + titleText);
				subTextView.click();
				break;
			}
		}

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement actionBarTexts = actionBar.findElement(By
				.className("android.widget.TextView"));
		String actionBarText = actionBarTexts.getText();
		System.out.println("=====进入" + actionBarText);

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 30))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement lay_container = driver.findElement(By
								.id("net.oschina.app:id/lay_container"));
						return lay_container;
					}
				});

		// 打印详情页面的标题
		String inforDetailsTitle = "";
		if (actionBarText.equals("软件详情") == true) {
			MobileElement tv_software_name = driver.findElement(By
					.id("net.oschina.app:id/tv_software_name"));
			inforDetailsTitle = tv_software_name.getText().substring(13);
			System.out.println("=====详情标题：" + inforDetailsTitle);
		} else if (actionBarText.equals("问答详情") == true) {
			MobileElement tv_ques_detail_title = driver.findElement(By
					.id("net.oschina.app:id/tv_ques_detail_title"));
			inforDetailsTitle = tv_ques_detail_title.getText();
			System.out.println("=====详情标题：" + inforDetailsTitle);
		} else {
			MobileElement tv_title = driver.findElement(By
					.id("net.oschina.app:id/tv_title"));
			inforDetailsTitle = tv_title.getText();
			System.out.println("=====详情标题：" + inforDetailsTitle);
		}

		// 进入开源资讯详情界面后截图
		this.screenshot("LoginNewsComment开源资讯详情界面.jpg", driver);

		// 点击添加评论
		MobileElement commentLinear = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement clickComment = commentLinear.findElement(By
				.id("net.oschina.app:id/tv_comment"));
		clickComment.click();

		// 输入评论内容，提交评论成功
		MobileElement commentLinear2 = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement sendComment = commentLinear2.findElement(By
				.id("net.oschina.app:id/et_comment"));
		sendComment.sendKeys("good");
		// 输入内容后，按回车键，确定输入内容
		String cmdstr = "adb shell input keyevent 66";
		try {
			Runtime.getRuntime().exec(cmdstr).waitFor();
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MobileElement subComment = driver.findElement(By
				.id("net.oschina.app:id/btn_comment"));
		subComment.click();
		// 输入评论后截图
		this.screenshot("LoginNewsComment输入评论.jpg", driver);
		Reporter.log("综合-开源资讯-登录-评论");

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
