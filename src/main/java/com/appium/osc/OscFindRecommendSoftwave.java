package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OscFindRecommendSoftwave extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "发现-开源软件-推荐-查看列表和详情信息")
	public void FindRecommendSoftwave() throws InterruptedException {
		// 点击【发现】
		MobileElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 点击【开源软件】
		MobileElement findSoft = driver.findElement(By
				.id("net.oschina.app:id/rl_soft"));
		findSoft.click();

		// 点击【推荐】
		MobileElement tabstrip = driver.findElement(By
				.id("net.oschina.app:id/pager_tabstrip"));
		List<MobileElement> tab_title = tabstrip.findElements(By
				.id("net.oschina.app:id/tab_title"));
		if (tab_title.size() > 0) {
			MobileElement getTitle = tab_title.get(1);
			getTitle.click();
		}

		// 等待30秒内寻找这个元素
		MobileElement getRecommendResult = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement listview = driver.findElement(By
								.id("net.oschina.app:id/listview"));
						List<MobileElement> tv_title = listview.findElements(By
								.id("net.oschina.app:id/tv_title"));
						if (tv_title.size() > 0) {
							MobileElement getRecommend = tv_title.get(0);
							return getRecommend;
						}
						return null;
					}
				});
		// 进入推荐列表截图
		this.screenshot("FindRecommendSoftwave开源软件推荐列表.jpg", driver);
		// 推荐-点击第一条推荐软件
		String RecommendText = getRecommendResult.getText();
		System.out.println("=====第一条推荐软件：" + RecommendText);
		getRecommendResult.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入推荐详情截图
		this.screenshot("FindRecommendSoftwave开源软件推荐详情.jpg", driver);
		// 打印详情页面的标题
		MobileElement tv_software_name = driver.findElement(By
				.id("net.oschina.app:id/tv_software_name"));
		System.out.println("=====详情标题：" + tv_software_name.getText());

		// 返回软件详情返回推荐
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		Reporter.log("发现-开源软件-推荐-查看列表和详情信息成功");

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
