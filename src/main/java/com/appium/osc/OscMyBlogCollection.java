package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscMyBlogCollection extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	// private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "我的博客-进入详情页面-收藏成功")
	public void MyBlogCollection() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【我的博客】
		MobileElement myNews = driver.findElement(By.name("我的博客"));
		myNews.click();
		// 进入博客列表页面
		MobileElement messageCenter = driver.findElement(By.name("用户博客列表"));
		System.out.println("=====进入" + messageCenter.getText());
		// 进入我的博客列表截图
		this.screenshot("MyBlogCollection我的博客列表.jpg", driver);
		// 遍历列表，点击第一条消息，进入博文内容
		MobileElement recyclerView1 = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> tv_titles = recyclerView1.findElements(By
				.id("net.oschina.app:id/tv_title"));
		MobileElement textView = tv_titles.get(0);
		String userNameText = textView.getText().substring(7);
		textView.click();

		// 进入博客详情页面
		MobileElement inforDetails = driver.findElement(By.name("博客详情"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入我的博客详情截图
		this.screenshot("MyBlogCollection我的博客详情.jpg", driver);
		// 检测是否进入对应博文标题的博客详情页面
		MobileElement inforDetails2 = driver.findElement(By.name(userNameText));
		System.out.println("=====进入" + inforDetails2.getText());

		// 收藏成功
		MobileElement clickCollection = driver.findElement(By
				.id("net.oschina.app:id/ib_fav"));
		clickCollection.click();
		String newsTitle = inforDetails2.getText();
		System.out.println("=====收藏" + newsTitle);
		// 点击收藏截图
		this.screenshot("MyBlogCollection点击收藏.jpg", driver);

		// 点击<，从博客详情返回博客列表界面
		MobileElement returnMyCollection = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickReturnMyCollection = returnMyCollection
				.findElement(By.className("android.widget.ImageButton"));
		clickReturnMyCollection.click();

		// 点击<，从博客列表返回个人信息界面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 检测收藏标题是否存在于我的收藏列表中
		OscCollectionResultAssert ResultAssert = new OscCollectionResultAssert();
		ResultAssert.CollectionResultAssert(driver, newsTitle);
		// 进入我的收藏列表截图
		this.screenshot("MyBlogCollection在我的收藏列表比对结果.jpg", driver);
		// 从我的收藏列表返回到个人信息页面
		MobileElement returnButton2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton2 = returnButton2.findElement(By
				.className("android.widget.ImageButton"));
		clickButton2.click();
		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
