package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OscFindNoLoginComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "发现-开源软件-推荐-未登录-登录-评论成功")
	public void FindNoLoginComment() throws InterruptedException {
		// 点击【发现】
		MobileElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 点击【开源软件】
		MobileElement findSoft = driver.findElement(By
				.id("net.oschina.app:id/rl_soft"));
		findSoft.click();
		// 进入发现开源软件列表界面后截图
		this.screenshot("FindNoLoginComment发现开源软件列表.jpg", driver);
		// 点击【推荐】
		MobileElement tabstrip = driver.findElement(By
				.id("net.oschina.app:id/pager_tabstrip"));
		List<MobileElement> tab_title = tabstrip.findElements(By
				.id("net.oschina.app:id/tab_title"));
		if (tab_title.size() > 0) {
			MobileElement getTitle = tab_title.get(1);
			getTitle.click();
		}

		// 等待30秒内寻找这个元素
		MobileElement getRecommendResult = (new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement listview = driver.findElement(By
								.id("net.oschina.app:id/listview"));
						List<MobileElement> tv_title = listview.findElements(By
								.id("net.oschina.app:id/tv_title"));
						if (tv_title.size() > 0) {
							MobileElement getRecommend = tv_title.get(2);
							return getRecommend;
						}
						return null;
					}
				});
		// 进入发现开源软件推荐列表界面后截图
		this.screenshot("FindNoLoginComment发现开源软件推荐列表.jpg", driver);
		// 推荐-点击第三条推荐软件
		String RecommendText = getRecommendResult.getText();
		System.out.println("=====第三条推荐软件：" + RecommendText);
		getRecommendResult.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement tv_software_name = driver.findElement(By
				.id("net.oschina.app:id/tv_software_name"));
		System.out.println("=====详情标题：" + tv_software_name.getText());
		// 进入发现开源软件推荐详情界面后截图
		this.screenshot("FindNoLoginComment发现开源软件推荐详情.jpg", driver);
		// 点击评论
		MobileElement lay_option = driver.findElement(By
				.id("net.oschina.app:id/lay_option"));
		MobileElement clickComment = lay_option.findElement(By
				.id("net.oschina.app:id/lay_option_comment"));
		clickComment.click();

		// 进入评论详情页面
		MobileElement actionBar2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails2 = actionBar2.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails2.getText());

		// 点击添加评论
		MobileElement ll_comment = driver.findElement(By
				.id("net.oschina.app:id/ll_comment"));
		MobileElement tv_comment = ll_comment.findElement(By
				.id("net.oschina.app:id/tv_comment"));
		tv_comment.click();

		// 判断是否登录，未登录，则要输入账号密码登录，才能评论内容
		MobileElement viewLogin = driver.findElement(By
				.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("====进入" + viewLogin.getText() + "页面=====");

		// 输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By
				.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginuUsername = driver.findElement(By
				.id("net.oschina.app:id/et_login_username"));
		loginuUsername.sendKeys("xxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By
				.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		MobileElement loginBtn = driver.findElement(By
				.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();
		// 登录后截图
		this.screenshot("FindNoLoginComment登录.jpg", driver);
		// 点击添加评论
		MobileElement ll_comment2 = driver.findElement(By
				.id("net.oschina.app:id/ll_comment"));
		MobileElement tv_comment2 = ll_comment2.findElement(By
				.id("net.oschina.app:id/tv_comment"));
		tv_comment2.click();

		// 输入评论内容，提交评论成功
		MobileElement commentLinear = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement sendComment = commentLinear.findElement(By
				.id("net.oschina.app:id/et_comment"));
		sendComment.sendKeys("good");

		// 输入内容后，按回车键，确定输入内容
		String cmdstr = "adb shell input keyevent 66";
		try {
			Runtime.getRuntime().exec(cmdstr).waitFor();
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MobileElement subComment = driver.findElement(By
				.id("net.oschina.app:id/btn_comment"));
		subComment.click();
		// 评论成功后截图
		this.screenshot("FindNoLoginComment评论成功.jpg", driver);
		Reporter.log("发现-开源软件-推荐-查看列表和详情信息-评论成功");

		// 从软件评论列表返回软件详情
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 从软件详情返回推荐列表
		MobileElement returnButton2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton2 = returnButton2.findElement(By
				.className("android.widget.ImageButton"));
		clickButton2.click();

		// 从推荐列表返回发现列表
		MobileElement returnButton3 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton3 = returnButton3.findElement(By
				.className("android.widget.ImageButton"));
		clickButton3.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
