package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscNewMove extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-最新动弹")
	public void NewMove() throws InterruptedException {
		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【最新动弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(0);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();
		// 等待5秒刷新页面
		 try{
		 Thread.sleep(5000);
		 }
		 catch(Exception e){
		 e.printStackTrace();
		 }

		// 等待30秒内寻找这个元素
		 MobileElement getTweetName = (new WebDriverWait(driver,30)).until(
		 new ExpectedCondition<MobileElement>(){
		 @Override
		 public MobileElement apply(WebDriver d) {
		     MobileElement recyclerView =
		 driver.findElement(By.id("net.oschina.app:id/recyclerView"));
		     List<MobileElement> LinearLayouts =
		 recyclerView.findElements(By.className("android.widget.LinearLayout"));
		 if(LinearLayouts.size()>0){
		     MobileElement textView = LinearLayouts.get(0);
		     MobileElement tv_tweet_name =textView.findElement(By.id("net.oschina.app:id/tv_tweet_name"));
		     return tv_tweet_name;
		 }
		     return null;
		 }
		 });
		// 进入最新动弹列表界面后截图
			this.screenshot("NewMove最新动弹列表.jpg", driver);
	
		// 等待20秒刷新页面
		// try{
		// Thread.sleep(20000);
		// }
		// catch(Exception e){
		// e.printStackTrace();
		// }

		// 点击第一条动弹
		String titleText = getTweetName.getText();
		System.out.println("=====动弹标题：" + titleText);
		getTweetName.click();

		// 进入动弹详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement inforDetailsContent = driver.findElement(By
				.id("net.oschina.app:id/tv_content"));
		System.out.println("=====详情标题：" + inforDetailsContent.getText());
		// 进入详情界面后截图
				this.screenshot("NewMove详情界面.jpg", driver);
		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
