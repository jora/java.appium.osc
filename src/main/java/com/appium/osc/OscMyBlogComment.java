package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscMyBlogComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	// private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "我的博客-进入详情页面-评论成功")
	public void MyBlogComment() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【我的博客】
		MobileElement myNews = driver.findElement(By.name("我的博客"));
		myNews.click();
		// 进入博客列表页面
		MobileElement messageCenter = driver.findElement(By.name("用户博客列表"));
		System.out.println("=====进入" + messageCenter.getText());
		// 进入我的博客列表截图
		this.screenshot("MyBlogComment我的博客列表.jpg", driver);
		// 遍历列表，点击第一条消息，进入私信内容
		List<MobileElement> linearLayouts = driver.findElements(By
				.className("android.widget.LinearLayout"));
		if (linearLayouts.size() > 0) {
			MobileElement textView = linearLayouts.get(1);
			MobileElement subTextView = textView.findElement(By
					.id("net.oschina.app:id/tv_title"));
			String userNameText = subTextView.getText().substring(7);
			subTextView.click();
			// 进入博客详情页面
			MobileElement inforDetails = driver.findElement(By.name("博客详情"));
			System.out.println("=====进入" + inforDetails.getText());
			// 进入我的博客详情截图
			this.screenshot("MyBlogComment我的博客详情.jpg", driver);
			// 检测是否进入对应博文标题的博客详情页面
			MobileElement inforDetails2 = driver.findElement(By
					.name(userNameText));
			System.out.println("=====进入" + inforDetails2.getText());
			// 点击添加评论
			MobileElement commentLinear = driver.findElement(By
					.className("android.widget.LinearLayout"));
			MobileElement clickComment = commentLinear.findElement(By
					.id("net.oschina.app:id/tv_comment"));
			clickComment.click();
			// 输入评论内容，提交评论成功
			MobileElement commentLinear2 = driver.findElement(By
					.className("android.widget.LinearLayout"));
			MobileElement sendComment = commentLinear2.findElement(By
					.id("net.oschina.app:id/et_comment"));
			sendComment.sendKeys("autotest");
			// HashMap<String, Integer> keycode = new HashMap<String,
			// Integer>();
			// keycode.put("keycode", 66);
			// ((JavascriptExecutor)driver).executeScript("mobile:keyevent",keycode);
			String cmdstr = "adb shell input keyevent 66";
			try {
				Runtime.getRuntime().exec(cmdstr).waitFor();
				Thread.sleep(5000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			MobileElement subComment = driver.findElement(By
					.id("net.oschina.app:id/btn_comment"));
			subComment.click();
			// 评论成功截图
			this.screenshot("MyBlogComment评论成功.jpg", driver);
			// 点击<，从博客详情返回博客列表界面
			MobileElement returnMyColletionComment = driver.findElement(By
					.id("net.oschina.app:id/action_bar"));
			MobileElement clickReturnMyinfor = returnMyColletionComment
					.findElement(By.className("android.widget.ImageButton"));
			clickReturnMyinfor.click();
		} else {
			System.out.println("====暂无内容====");
		}

		Reporter.log("我的博客-进入博客详情页面-评论成功");

		// 点击<，从博客列表返回个人信息界面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
