package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscViewRecommend extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "综合-推荐博客")
	public void ViewRecommend() throws InterruptedException {
		// 点击【综合】
		MobileElement itemMe = driver.findElement(By
				.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();
		// 点击【推荐博客】
		MobileElement layoutView = driver.findElement(By
				.id("net.oschina.app:id/layout_tab"));
		MobileElement linearLayout = layoutView.findElement(By
				.className("android.widget.LinearLayout"));
		List<MobileElement> support = linearLayout.findElements(By
				.className("android.support.v7.a.a$f"));
		MobileElement supportText = support.get(1);
		supportText.click();

		// 等待30秒内寻找这个元素
		 (new WebDriverWait(driver,30)).until(
		 new ExpectedCondition<MobileElement>(){
		 @Override
		 public MobileElement apply(WebDriver d) {
		 MobileElement recyclerView =
		 driver.findElement(By.id("net.oschina.app:id/recyclerView"));
		 List<MobileElement> linearLayout2 =
		 recyclerView.findElements(By.className("android.widget.LinearLayout"));
		 if(linearLayout2.size()>0){
		 MobileElement textView=linearLayout2.get(0);
		 MobileElement subTextView =
		 textView.findElement(By.id("net.oschina.app:id/tv_title"));
		 return subTextView;
		 }
		 return null;
		 }
		 });
		 //进入推荐博客后截图
		 this.screenshot("ViewRecommend推荐博客列表.jpg",driver);
	
		// 等待20秒刷新页面
		// try{
		// Thread.sleep(20000);
		// }
		// catch(Exception e){
		// e.printStackTrace();
		// }

		// 点击第一条博客
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> linearLayout2 = recyclerView.findElements(By
				.className("android.widget.LinearLayout"));
		Assert.assertNotEquals(linearLayout2.size(), 0);
		MobileElement textView = linearLayout2.get(0);
		MobileElement subTextView = textView.findElement(By
					.id("net.oschina.app:id/tv_title"));
		String titleText = subTextView.getText();
        int index = titleText.lastIndexOf("]");
        if(index > -1){
        	titleText = titleText.substring(index + 2);
        }
		System.out.println("=====列表标题：" + titleText);
		subTextView.click();
		
		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement inforDetailsTitle = driver.findElement(By
				.id("net.oschina.app:id/tv_title"));
		Assert.assertEquals(inforDetailsTitle.getText(),titleText);
		if(inforDetailsTitle.getText().equals(titleText) == true){
			System.out.println("====详情标题：" + inforDetailsTitle.getText());
			Reporter.log("查看推荐博客列表和详情信息成功");
		}else{
			Reporter.log("查看推荐博客列表和详情信息失败");
		}
	    //进入推荐博客详情界面后截图
	    this.screenshot("ViewRecommend推荐博客详情界面.jpg",driver);

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
