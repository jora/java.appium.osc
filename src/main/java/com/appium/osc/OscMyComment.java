package com.appium.osc;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OscMyComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	// private boolean isInstall = false;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "我的消息-评论")
	public void MyComment() throws InterruptedException {
		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);

		// 点击【我的消息】
		MobileElement myNews = driver.findElement(By.name("我的消息"));
		myNews.click();
		// 进入消息中心页面
		MobileElement messageCenter = driver.findElement(By.name("消息中心"));
		System.out.println("=====进入" + messageCenter.getText());
		// 进入消息中心截图
		this.screenshot("MyComment消息中心.jpg", driver);
		// 点击评论
		MobileElement myComment = driver.findElement(By.name("评论"));
		myComment.click();
		System.out.println("==进入我的评论列表==");
		// 进入评论列表截图
		this.screenshot("MyComment评论列表.jpg", driver);
		// 遍历列表，点击第一条消息，进入评论内容
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> linearLayouts = recyclerView.findElements(By
				.className("android.widget.LinearLayout"));
		Assert.assertNotEquals(linearLayouts.size(), 0);
			MobileElement textView = linearLayouts.get(0);
			MobileElement subTextView = textView.findElement(By
					.id("net.oschina.app:id/tv_content"));
			String userNameText = subTextView.getText();
			subTextView.click();
			// 进入动弹详情页面
			MobileElement inforDetails = driver.findElement(By
					.name(userNameText));
			System.out.println("=====进入" + inforDetails.getText());
			// 进入评论内容截图
			this.screenshot("MyComment评论内容.jpg", driver);
			// 点击<，从动弹详情返回消息中心界面
			MobileElement returnMyComment = driver.findElement(By
					.id("net.oschina.app:id/toolbar"));
			MobileElement clickReturnMyComment = returnMyComment.findElement(By
					.className("android.widget.ImageButton"));
			clickReturnMyComment.click();
		// } else {
		// System.out.println("====暂无内容====");
		// }

		Reporter.log("我的消息-评论");

		// 点击<，从消息中心返回个人信息界面
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
