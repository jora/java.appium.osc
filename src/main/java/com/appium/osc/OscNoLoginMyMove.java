package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class OscNoLoginMyMove extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "动弹-我的动弹")
	public void NoLoginMyMove() throws InterruptedException {
		// 点击【动弹】
		MobileElement itemTweet = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet.click();
		// 点击【我的动弹】
		MobileElement tab_nav = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support = tab_nav.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText = support.get(3);
		System.out.println("====进入" + supportText.getText() + "成功");
		supportText.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 30))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement layoutError = driver.findElement(By
								.id("net.oschina.app:id/layout_error"));
						MobileElement errorText = layoutError.findElement(By
								.id("net.oschina.app:id/tv_error_layout"));
						return errorText;
					}
				});

		// 登录
		OscLogin Login = new OscLogin();
		Login.login(driver);
		// 登录后截图
		this.screenshot("NoLoginMyMove登录成功.jpg", driver);

		// 点击【动弹】
		MobileElement itemTweet2 = driver.findElement(By
				.id("net.oschina.app:id/nav_item_tweet"));
		itemTweet2.click();
		// 点击【我的动弹】
		MobileElement tab_nav2 = driver.findElement(By
				.id("net.oschina.app:id/tab_nav"));
		List<MobileElement> support2 = tab_nav2.findElements(By
				.className("android.widget.TextView"));
		MobileElement supportText2 = support2.get(3);
		System.out.println("====进入" + supportText2.getText() + "成功");
		supportText2.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 30))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement recyclerView = driver.findElement(By
								.id("net.oschina.app:id/recyclerView"));
						List<MobileElement> tweet_Item = recyclerView
								.findElements(By
										.id("net.oschina.app:id/tweet_item"));
						if (tweet_Item.size() > 0) {
							MobileElement textView = tweet_Item.get(0);
							return textView;
						}
						return null;
					}
				});
		// 进入我的动弹列表截图
		this.screenshot("NoLoginMyMove我的动弹列表.jpg", driver);
		// 点击第一条动弹
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> tweet_Item = recyclerView.findElements(By
				.id("net.oschina.app:id/tweet_item"));
		if (tweet_Item.size() > 0) {
			MobileElement textView = tweet_Item.get(0);
			String titleText = textView.getText();
			System.out.println("=====动弹标题：" + titleText);
			textView.click();
		}

		// 进入动弹详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());

		// 打印详情页面的标题
		MobileElement Container = driver.findElement(By
				.id("net.oschina.app:id/layout_container"));
		MobileElement inforDetailsContent = Container.findElement(By
				.id("net.oschina.app:id/tv_content"));
		System.out.println("=====详情标题：" + inforDetailsContent.getText());
		Reporter.log("动弹-我的动弹");
		// 进入动弹详情截图
		this.screenshot("NoLoginMyMove我的动弹详情.jpg", driver);
		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/toolbar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
