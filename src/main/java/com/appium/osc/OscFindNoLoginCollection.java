package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OscFindNoLoginCollection extends BaseTest{

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {
		
		driver = this.getAppiumParameter();

	}

	@Test(description="发现-开源软件-编程语言-java-Spring-未登录-登录-收藏成功")
	public void FindNoLoginCollection() throws InterruptedException {
		// 点击【发现】
		WebElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 点击【开源软件】
		WebElement findSoft = driver.findElement(By
				.id("net.oschina.app:id/rl_soft"));
		findSoft.click();
		
		// 点击【分类】
		WebElement tabstrip 
		 = driver.findElement(By.id("net.oschina.app:id/pager_tabstrip"));
			List<WebElement> tab_title 
			= tabstrip.findElements(By.id("net.oschina.app:id/tab_title"));
			if(tab_title.size()>0){
			   WebElement getTitle=tab_title.get(0);
			   getTitle.click();
			}
		   
		// 等待30秒内寻找这个元素
		WebElement getSoftware =(new WebDriverWait(driver,20)).until(
		new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver d) {
			   WebElement lv_catalog 
				= driver.findElement(By.id("net.oschina.app:id/lv_catalog"));
				List<WebElement> software_catalog_name 
				= lv_catalog.findElements(By.id("net.oschina.app:id/tv_software_catalog_name"));
				  if(software_catalog_name.size()>0){
					 WebElement getSoftware1=software_catalog_name.get(0);
			         return getSoftware1;
			         }
			      return null;
			 }
		});	   
		// 进入分类列表界面后截图
		this.screenshot("FindNoLoginCollection分类列表.jpg", driver);	 
		// 分类-第一分类【编程语言】
		String banTitleText = getSoftware.getText();
		System.out.println("=====第一分类：" + banTitleText);
		getSoftware.click();
		
		// 等待30秒内寻找这个元素
		WebElement getSoftJava =(new WebDriverWait(driver,20)).until(
		new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver d) {
			   WebElement lv_tag 
				= driver.findElement(By.id("net.oschina.app:id/lv_tag"));
				List<WebElement> software_catalog_name2 
				= lv_tag.findElements(By.id("net.oschina.app:id/tv_software_catalog_name"));
				  if(software_catalog_name2.size()>0){
					 WebElement getSoftware2=software_catalog_name2.get(0);
			         return getSoftware2;
			         }
			      return null;
			 }
		});	   
		// 进入编程语言列表界面后截图
		this.screenshot("FindNoLoginCollection编程语言列表.jpg", driver);	 
		// 分类-第一分类【编程语言】-点击【java】
		String softJavaText = getSoftJava.getText();
		System.out.println("=====第一种语言类：" + softJavaText);
		getSoftJava.click();
	
		// 等待30秒内寻找这个元素
		WebElement getSpring =(new WebDriverWait(driver,20)).until(
		new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver d) {
			   WebElement lv_software 
				= driver.findElement(By.id("net.oschina.app:id/lv_software"));
				List<WebElement> tv_title 
				= lv_software.findElements(By.id("net.oschina.app:id/tv_title"));
				  if(tv_title.size()>0){
					 WebElement getSoftware3=tv_title.get(1);
			         return getSoftware3;
			         }
			      return null;
			 }
		});	   
		// 进入语言类列表界面后截图
		this.screenshot("FindNoLoginCollection语言类列表.jpg", driver);	 
		// 分类-第一分类【编程语言】-点击【java】-点击Spring
		String softText = getSpring.getText();
		System.out.println("=====第一种语言：" + softText);
		getSpring.click();
		
		//进入详情页面
		WebElement actionBar = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		WebElement inforDetails = actionBar.findElement(By.className("android.widget.TextView"));
	    System.out.println("=====进入" + inforDetails.getText());
		   
		 //打印详情页面的标题
		 WebElement tv_software_name = driver.findElement(By.id("net.oschina.app:id/tv_software_name"));
			String software_name = tv_software_name.getText();
			if (software_name.indexOf(softText)!= -1) {
				System.out.println("=====详情标题：" + tv_software_name.getText());
			} else {
				System.out.println("==进入详情页面失败==");
			}
		// 进入某种语言详情界面后截图
		this.screenshot("FindNoLoginCollection语言详情界面.jpg", driver);
			
		//点击收藏
		WebElement lay_option = driver.findElement(By.id("net.oschina.app:id/lay_option"));
		WebElement clickCollection = lay_option.findElement(By.id("net.oschina.app:id/lay_option_fav"));
		clickCollection.click();
		
		//判断是否登录，未登录，则要输入账号密码登录，才能评论内容
		WebElement viewLogin = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		System.out.println("====进入" + viewLogin.getText() + "页面=====");
		
		//输入账号和密码，登录
		WebElement delUsername = driver.findElement(By.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		WebElement loginuUsername = driver.findElement(By.id("net.oschina.app:id/et_login_username"));
		loginuUsername.sendKeys("xxxxxx@163.com");
		WebElement loginuPwd = driver.findElement(By.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		WebElement loginBtn = driver.findElement(By.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();				
	
		// 进入登录后截图
		this.screenshot("FindNoLoginCollection登录.jpg", driver);
		
		 //收藏成功
		WebElement lay_option2 = driver.findElement(By.id("net.oschina.app:id/lay_option"));
		WebElement clickCollection2 = lay_option2.findElement(By.id("net.oschina.app:id/lay_option_fav"));
		clickCollection2.click();
		String newsTitle = tv_software_name.getText();
		System.out.println("==收藏：" + newsTitle);
		// 收藏成功截图
		this.screenshot("FindNoLoginCollection收藏成功.jpg", driver);
			 
		// 从软件详情返回分类
		WebElement returnButton = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		WebElement clickButton = returnButton.findElement(By.className("android.widget.ImageButton"));
		clickButton.click();
		
		// 点击【推荐】
		WebElement tabstrip2 
		 = driver.findElement(By.id("net.oschina.app:id/pager_tabstrip"));
			List<WebElement> tab_title2 
			= tabstrip2.findElements(By.id("net.oschina.app:id/tab_title"));
			if(tab_title2.size()>0){
			   WebElement getTitle2 = tab_title2.get(1);
			   getTitle2.click();
			}
			
		// 从推荐返回到发现列表界面
		WebElement returnButton2 = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		WebElement clickButton2 = returnButton2.findElement(By.className("android.widget.ImageButton"));
		clickButton2.click();
		
		// 检测收藏标题是否存在于我的收藏列表中
		OscCollectionResultAssert ResultAssert = new OscCollectionResultAssert();
		ResultAssert.CollectionResultAssert(driver, softText);
		// 进入我的收藏列表后截图
		this.screenshot("FindLoginCollection我的收藏列表.jpg", driver);
		Reporter.log("发现-开源软件-java-Spring-查看软件详情信息-未登录-登录收藏成功");

		// 从我的收藏列表返回到个人信息页面
		MobileElement returnButton3 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton3 = returnButton3.findElement(By
				.className("android.widget.ImageButton"));
		clickButton3.click();
		
		//注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);

	}

	@AfterClass
	public void tearDown() throws Exception {
		 driver.quit();
	}


	
}
