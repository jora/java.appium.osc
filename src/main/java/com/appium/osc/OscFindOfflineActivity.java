package com.appium.osc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OscFindOfflineActivity extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "发现-线下活动")
	public void FindOfflineActivity() throws InterruptedException {
		// 点击【发现】
		MobileElement finditem = driver.findElement(By
				.id("net.oschina.app:id/nav_item_explore"));
		finditem.click();
		// 进入发现列表截图
		this.screenshot("FindOfflineActivity发现列表.jpg", driver);
		// 点击【线下活动】
		MobileElement findAct = driver.findElement(By
				.id("net.oschina.app:id/layout_events"));
		findAct.click();

		// 等待30秒内寻找这个元素
		(new WebDriverWait(driver, 20))
				.until(new ExpectedCondition<MobileElement>() {
					@Override
					public MobileElement apply(WebDriver d) {
						MobileElement recyclerView = driver.findElement(By
								.id("net.oschina.app:id/recyclerView"));
						List<MobileElement> linearLayout = recyclerView.findElements(By
								.className("android.widget.LinearLayout"));
						if (linearLayout.size() > 0) {
							MobileElement getLinear = linearLayout.get(1);
							MobileElement event_title = getLinear.findElement(By
									.id("net.oschina.app:id/tv_event_title"));
							return event_title;
						}
						return null;
					}
				});
		// 进入发现-线下活动列表截图
		this.screenshot("FindOfflineActivity发现线下活动列表.jpg", driver);
		// 点击滚动条的活动
		MobileElement vpBanner = driver.findElement(By
				.id("net.oschina.app:id/vp_banner"));
		MobileElement bannerTitle = vpBanner.findElement(By
				.id("net.oschina.app:id/tv_event_banner_title"));
		String banTitleText = bannerTitle.getText().substring(7);
		System.out.println("=====列表标题：" + banTitleText);
		bannerTitle.click();

		// 进入详情页面
		MobileElement actionBar = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails = actionBar.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails.getText());
		// 进入发现-线下活动-滚动条活动截图
		this.screenshot("FindOfflineActivity发现线下活动滚动条活动.jpg", driver);
		// 打印详情页面的标题
		MobileElement inforDetailsTitle = driver.findElement(By
				.id("net.oschina.app:id/tv_event_title"));
		System.out.println("=====详情标题：" + inforDetailsTitle.getText());
		// 返回线下活动列表
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();
		Reporter.log("发现-线下活动-查看滚动条的活动成功");

		// 点击第一条线下活动
		MobileElement recyclerView = driver.findElement(By
				.id("net.oschina.app:id/recyclerView"));
		List<MobileElement> linearLayout = recyclerView.findElements(By
				.className("android.widget.LinearLayout"));
		if (linearLayout.size() > 0) {
			MobileElement getLinear = linearLayout.get(1);
			MobileElement event_title = getLinear.findElement(By
					.id("net.oschina.app:id/tv_event_title"));
			String titleText = event_title.getText().substring(7);
			System.out.println("=====列表标题：" + titleText);
			event_title.click();
		}

		// 进入详情页面
		MobileElement actionBar2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement inforDetails2 = actionBar2.findElement(By
				.className("android.widget.TextView"));
		System.out.println("=====进入" + inforDetails2.getText());
		// 进入发现-线下活动-第一条线下活动截图
		this.screenshot("FindOfflineActivity发现线下活动第一条线下活动.jpg", driver);
		// 打印详情页面的标题
		MobileElement inforDetailsTitle2 = driver.findElement(By
				.id("net.oschina.app:id/tv_event_title"));
		System.out.println("=====详情标题：" + inforDetailsTitle2.getText());
		// 返回首页
		MobileElement returnButton2 = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton2 = returnButton2.findElement(By
				.className("android.widget.ImageButton"));
		clickButton2.click();
		Reporter.log("发现-线下活动-查看第一条线下活动成功");

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
