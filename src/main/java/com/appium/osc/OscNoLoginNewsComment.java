package com.appium.osc;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

public class OscNoLoginNewsComment extends BaseTest {

	private AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void setUp() throws Exception {

		driver = this.getAppiumParameter();

	}

	@Test(description = "综合-开源资讯-未登录-登录-评论成功")
	public void NoLoginNewsComment() throws InterruptedException {
		// 点击【综合】
		MobileElement itemMe = driver.findElement(By
				.id("net.oschina.app:id/nav_item_news"));
		itemMe.click();
		// 点击【开源资讯】
		MobileElement layoutView = driver.findElement(By
				.id("net.oschina.app:id/layout_tab"));
		MobileElement linearLayout = layoutView.findElement(By
				.className("android.widget.LinearLayout"));
		List<MobileElement> support = linearLayout.findElements(By
				.className("android.support.v7.a.a$f"));
		MobileElement supportText = support.get(0);
		supportText.click();

		// 等待30秒内寻找这个元素
		   (new WebDriverWait(driver,20)).until(
			new ExpectedCondition<MobileElement>(){
				@Override
				public MobileElement apply(WebDriver d) {
					MobileElement recyclerView = driver.findElement(By
							.id("net.oschina.app:id/recyclerView"));
					List<MobileElement> titleView = recyclerView.findElements(By
							.id("net.oschina.app:id/ll_title"));
					   if(titleView.size()>0){
					   MobileElement textView=titleView.get(0);
					   return textView;
					   }
					   return null;
				 }
			});		
		 //进入开源资讯列表界面后截图
		   this.screenshot("NoLoginNewsComment开源资讯列表.jpg",driver);		   
		// 点击资讯标题
		   MobileElement recyclerView = driver.findElement(By
					.id("net.oschina.app:id/recyclerView"));
			List<MobileElement> titleView = recyclerView.findElements(By
					.id("net.oschina.app:id/ll_title"));
//			String titleText = "";
			for(int i=0;i<titleView.size();i++){
				MobileElement textView=titleView.get(i);				
				MobileElement lay_info = textView.findElement(By.id("net.oschina.app:id/lay_info"));
				if(lay_info != null){
				MobileElement subTextView = textView.findElement(By
						   .id("net.oschina.app:id/tv_title"));
				   String titleText = subTextView.getText().substring(7);
				   System.out.println("=====列表标题：" + titleText );
				   subTextView.click();
				   break;
				}
			}
		      
		   
		 //进入详情页面
		   MobileElement actionBar = driver.findElement(By.id("net.oschina.app:id/action_bar"));
		   MobileElement actionBarTexts = actionBar.findElement(By.className("android.widget.TextView"));
		   String actionBarText = actionBarTexts.getText();
		   System.out.println("=====进入" + actionBarText);
		  
			// 等待30秒内寻找这个元素
		   (new WebDriverWait(driver,30)).until(
			new ExpectedCondition<MobileElement>(){
				@Override
				public MobileElement apply(WebDriver d) {
					MobileElement lay_container = driver.findElement(By
							.id("net.oschina.app:id/lay_container"));				
				      return lay_container;
				 }
			});	  
		   
		   //打印详情页面的标题
		   String inforDetailsTitle = "";
		   if(actionBarText.equals("软件详情") == true){
			   MobileElement tv_software_name = driver.findElement(By
					   .id("net.oschina.app:id/tv_software_name"));
			   inforDetailsTitle = tv_software_name.getText().substring(13);
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else if(actionBarText.equals("问答详情") == true){
			   MobileElement tv_ques_detail_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_ques_detail_title"));
			   inforDetailsTitle = tv_ques_detail_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }else{
			   MobileElement tv_title = driver.findElement(By
					   .id("net.oschina.app:id/tv_title"));
			   inforDetailsTitle = tv_title.getText();
			   System.out.println("=====详情标题：" + inforDetailsTitle);
		   }
		
		//刷新页面等待3秒
	    try{
	    	Thread.sleep(3000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    //进入开源资讯详情界面后截图
	    this.screenshot("NoLoginNewsComment开源资讯详情界面.jpg",driver);

		// 点击添加评论
		MobileElement commentLinear = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement clickComment = commentLinear.findElement(By
				.id("net.oschina.app:id/tv_comment"));
		clickComment.click();
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		// 判断是否登录，未登录，则要输入账号密码登录，才能评论内容
	    (new WebDriverWait(driver,30)).until(ExpectedConditions.elementToBeClickable(By.id("net.oschina.app:id/iv_login_logo")));
//		MobileElement iv_login_logo = driver.findElement(By
//				.id("net.oschina.app:id/iv_login_logo"));
		System.out.println("====进入登录页面=====");

		// 输入账号和密码，登录
		MobileElement delUsername = driver.findElement(By
				.id("net.oschina.app:id/iv_login_username_del"));
		delUsername.click();
		MobileElement loginuUsername = driver.findElement(By
				.id("net.oschina.app:id/et_login_username"));
		loginuUsername.sendKeys("xxxxx@163.com");
		MobileElement loginuPwd = driver.findElement(By
				.id("net.oschina.app:id/et_login_pwd"));
		loginuPwd.clear();
		loginuPwd.sendKeys("123456");
		MobileElement loginBtn = driver.findElement(By
				.id("net.oschina.app:id/bt_login_submit"));
		loginBtn.click();
	    //进入登录后截图
	    this.screenshot("NoLoginNewsComment登录.jpg",driver);
	    
		// 点击添加评论
		MobileElement commentLinear2 = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement clickComment2 = commentLinear2.findElement(By
				.id("net.oschina.app:id/tv_comment"));
		clickComment2.click();

		// 输入评论内容，提交评论成功
		MobileElement commentLinearLayout = driver.findElement(By
				.className("android.widget.LinearLayout"));
		MobileElement sendComment = commentLinearLayout.findElement(By
				.id("net.oschina.app:id/et_comment"));
		sendComment.sendKeys("good");
	    //输入评论后截图
	    this.screenshot("NoLoginNewsComment输入评论.jpg",driver);
	    
		// 输入内容后，按回车键，确定输入内容
		String cmdstr = "adb shell input keyevent 66";
		try {
			Runtime.getRuntime().exec(cmdstr).waitFor();
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MobileElement subComment = driver.findElement(By
				.id("net.oschina.app:id/btn_comment"));
		subComment.click();
		//刷新页面等待5秒
	    try{
	    	Thread.sleep(5000);
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    System.out.println("===提交评论内容成功===");
	    //提交评论内容后截图
	    this.screenshot("NoLoginNewsComment提交评论成功.jpg",driver);
	    
	    Reporter.log("综合-开源资讯-未登录-登录-评论");
	    
//		// 点击【评论列表】图标
//		MobileElement action_bar = driver.findElement(By
//				.id("net.oschina.app:id/action_bar"));
//		MobileElement clickCommentCount = action_bar.findElement(By
//				.id("net.oschina.app:id/tv_comment_count"));
//		clickCommentCount.click();
//	    
//        //进入评论列表
//        MobileElement detail_comment = driver.findElement(By
//				.id("net.oschina.app:id/lay_blog_detail_comment"));
//        List<MobileElement> tv_content = detail_comment
//				.findElements(By
//						.id("net.oschina.app:id/tv_content"));
//		Assert.assertNotEquals(tv_content.size(), 0);
//		MobileElement getContent = tv_content.get(0);
//        String getContentTitle = getContent.getText();
//        
//		//比较评论内容是否一致
//        Assert.assertEquals(getContentTitle,"autotest");
//        if(getContentTitle.equals("autotest") == true){
//        	System.out.print("比较输入评论内容正确");
//        	Reporter.log("综合-开源资讯-未登录-登录-评论成功");
//        }else{
//        	Reporter.log("综合-开源资讯-未登录-登录-评论失败");
//        }
//        // 从评论列表返回博客详情页面
//        MobileElement back_label = driver.findElement(By
//     				.id("net.oschina.app:id/tv_back_label"));
//        back_label.click();

		// 返回首页
		MobileElement returnButton = driver.findElement(By
				.id("net.oschina.app:id/action_bar"));
		MobileElement clickButton = returnButton.findElement(By
				.className("android.widget.ImageButton"));
		clickButton.click();

		// 注销
		OscCancellation Cancellation = new OscCancellation();
		Cancellation.Cancellation(driver);
	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
